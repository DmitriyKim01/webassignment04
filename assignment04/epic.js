"use strict"

/**
 * Runs the setup function to the DOMContentLoaded event.
 */

document.addEventListener ("DOMContentLoaded", setup);

/**
 * Sets up the initial state of the application.
 */

function setup () {
    let type = document.querySelector('#type').value;
    let ulist = document.querySelector ('#image-menu') ;
    let form = document.querySelector('#request_form');
    restrictDates('natural');
    let array = [];

    form.addEventListener('submit', function (event) {
        event.preventDefault();
        type = document.querySelector('#type').value;
        const template = document.querySelector('#image-menu-item');
        let date = document.querySelector('#date').value;
        ulist.textContent = undefined;
        array = [];
        fetch (`https://epic.gsfc.nasa.gov/api/${type}/date/${date}`)
        .then(response =>  {
            if (response.ok) {
                console.log ("Response : SUCCESS", response);
            }
            else {
                console.log ("Response : FAILLURE", {cause : response});
                addNoImageMessage(ulist, template);
            }
            return response.json();
        })
        .then(data => {
            if (data.length === 0 || data.length === null){
                addNoImageMessage(ulist, template);
            }
            else{
                data.forEach(element => {
                    array.push(element);
                });
                addImageToList (array, ulist, template);
            }
        })
        .catch(error => console.log('Error!', error))
    });

    ulist.addEventListener('click', (event) => {
        let target = event.target.parentNode;
        let clickedObj = array[target.getAttribute('data-image-list-index')];
        let imageURL = urlForImage(clickedObj, type);

        let dateDiv = document.querySelector('#earth-image-date');
        let captionDiv = document.querySelector('#earth-image-title');
        setHeaders(clickedObj.date, clickedObj.caption, dateDiv, captionDiv, );

        let image = document.querySelector('#earth-image');
        setImage(imageURL, image);
    });

    const select = document.querySelector('#type');
    select.addEventListener('change', (e) => {
        let imageType = document.querySelector('#type').value;
        restrictDates(imageType);
    });
}

/**
 * Fetches the available dates for the given image type and updates the date input field accordingly.
 * @param {string} type 
 */

function restrictDates (type) {
    fetch (`https://epic.gsfc.nasa.gov/api/${type}/all`)
    .then(response =>  {
        if (response.ok) {
            console.log ("Response : SUCCESS", response);
        }
        else {
            console.log ("Response : FAILLURE", {cause : response});
        }
        return response.json();
    })
    .then(dates => {
        let listOfDates = [];
        dates.forEach(obj => {
            listOfDates.push(obj.date.toString());
        });
        let sortedDates = listOfDates.sort((a, b) => b - a);
        let latestDate = sortedDates[0];
        let dateInput = document.querySelector('#date');
        dateInput.max = latestDate;
    })
    .catch(error => console.log('Error!', error))
}

/**
 * Adds a message to the list indicating that no images were found. 
 * @param {HTMLUListElement} ulist 
 * @param {HTMLTemplateElement} template 
 */

function addNoImageMessage (ulist, template) {
    ulist.textContent = undefined;
    const content = template.content;
    let clone = template.content.cloneNode(true);
    let list = clone.querySelector('li');
    const span = list.querySelector('.image-list-title');
    list.removeChild(span);
    list.textContent = "No images found!";
    ulist.appendChild(clone);
}

/**
 * Add headers of images to the list.
 * @param {Array} array 
 * @param {HTMLUListElement} ulist 
 * @param {HTMLTemplateElement} template 
 */

function addImageToList (array, ulist, template) {
    ulist.textContent = undefined;
    for (let i = 0; i < array.length; i++){
        let clone = template.content.cloneNode(true);
        let list = clone.querySelector('li');
        list.setAttribute('data-image-list-index', i);
        let span = clone.querySelector('.image-list-title');
        span.textContent = array[i].date;
        ulist.appendChild(clone);
    }
}

/**
 * Constructs the URL for the given image object and type.
 * @param {Object} obj 
 * @param {string} type 
 * @returns {string} 
 */

function urlForImage (obj, type) {
    let imgDate = obj.date.split(' ');
    imgDate = imgDate[0];
    let formatedDate = imgDate.replace(/-/g, '/');
    let url = `https://epic.gsfc.nasa.gov/archive/${type}/${formatedDate}/jpg/${obj.image}.jpg`;
    return url;
}

/**
 * Sets the date and caption headers for the displayed image.
 * @param {string} date  
 * @param {string} caption 
 * @param {HTMLElement} dateDiv 
 * @param {HTMLElement} captionDiv 
 */

function setHeaders (date, caption, dateDiv, captionDiv) {
    dateDiv.textContent = "";
    let h2 = document.createElement('h2');
    h2.textContent = date;
    dateDiv.appendChild(h2);

    captionDiv.textContent = "";
    let paragraph = document.createElement('p');
    paragraph.textContent = caption;
    captionDiv.appendChild(paragraph);
}

function setImage (url, image) {
    image.src = url;
}



